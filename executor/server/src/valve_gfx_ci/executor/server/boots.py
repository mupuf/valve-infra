from threading import Thread
import hashlib
import socket
import logging
import time

from . import config
from .android.fastbootd import Fastbootd
from .tftpd import TFTPD, TftpRequest, TftpRequestFileNotFoundHandler
from .dhcpd import DHCPD, MacAddress, DhcpOptions
from .logger import logger
from .socketactivation import get_sockets_by_name


class BootsDHCPD(DHCPD, Thread):
    def __init__(self, boots, name, interface):  # pragma: nocover
        self.boots = boots

        Thread.__init__(self, name=name)
        self.daemon = True

        DHCPD.__init__(self, interface=interface)

    @property
    def static_clients(self):
        clients = []
        for device in self.boots.mars.known_ethernet_devices:
            if device.ip_address and device.mac_address:
                clients.append({'mac_addr': MacAddress(device.mac_address),
                                'ipaddr': device.ip_address,
                                'hostname': device.hostname})
        return clients

    def get_response(self, client_request) -> DhcpOptions:
        mac_addr = client_request.mac_addr
        if client_request.is_valid_netboot_request:
            # Ensure MaRS is aware of this client request

            ip_address, _ = self.get_or_assign_ip_for_client(mac_addr)
            arch = client_request.architecture.name
            fw = client_request.firmware.name
            protocol = client_request.protocol.name
            dut_data = {
                "id": mac_addr.as_str,
                "mac_address": mac_addr.as_str,
                "base_name": f"{arch}-{fw}-{protocol}".lower(),
                "ip_address": ip_address,
                "tags": []
            }
            self.boots.mars.machine_discovered(dut_data, update_if_already_exists=False)

        # Forward the request to the job
        # TODO: Make this request in the background?
        if dut := self.boots.mars.get_machine_by_id(mac_addr.as_str, raise_if_missing=False):
            if response := dut.handle_dhcp_request(client_request):
                return response
            else:
                self.logger.info("The associated job did not return a DHCP configuration, use defaults!")

        # The job did not tell us what to do, so let's fallback to our defaults
        return DhcpOptions()

    def run(self):  # pragma: nocover
        # Use the socket provided by our caller (systemd?), or create or own if none are found
        sock = None
        if sockets := get_sockets_by_name(config.BOOTS_DHCP_IPv4_SOCKET_NAME, socket.AF_INET, socket.SOCK_DGRAM):
            sock = sockets[0]

        self.listen(sock)


class BootsTFTPD(TFTPD, Thread):  # pragma: nocover
    def __init__(self, boots, name, directory, interface):
        self.boots = boots

        Thread.__init__(self, name=name)
        self.daemon = True

        tftp_logger = logging.getLogger(f"{logger.name}.TFTP")
        tftp_logger.setLevel(logging.INFO)
        TFTPD.__init__(self, interface, logger=tftp_logger, netboot_directory=directory)

    def new_request(self, request: TftpRequest):
        self.logger.info(f"Received the {request.opcode.name} TFTP request for {request.filename}")

        # TODO: Make this request in the background?
        if dut := self.boots.mars.get_machine_by_ip_address(request.client_address, raise_if_missing=False):
            if dut.handle_tftp_request(request):
                # We successfully handled the request, nothing else to do!
                opcode_name = request.opcode.name
                self.logger.info(f"--> Processed the {opcode_name},{request.filename} TFTP request by the related job")
                return
            else:
                reason = "Unhandled by job"
        else:
            reason = "No associated DUT"

        # We could not get the executor job to handle the query, handle it ourselves and simply return FileNotFound
        self.logger.info(f"--> Ignore the {request.opcode.name},{request.filename} TFTP request ({reason})")
        TftpRequestFileNotFoundHandler(request, netboot_directory=self.netboot_directory,
                                       default_retries=self.default_retries, timeout=self.timeout,
                                       parent=self)

    def run(self):
        # Use the socket provided by our caller (systemd?), or create or own if none are found
        sock = None
        if sockets := get_sockets_by_name(config.BOOTS_TFTP_IPv4_SOCKET_NAME, socket.AF_INET, socket.SOCK_DGRAM):
            sock = sockets[0]

        self.listen(sock)


class BootsFastbootd(Fastbootd, Thread):  # pragma: nocover
    def __init__(self, boots, name):
        self.boots = boots

        Thread.__init__(self, name=name)
        self.daemon = True

        fastbootd_logger = logging.getLogger(f"{logger.name}.Fastbootd")
        fastbootd_logger.setLevel(logging.INFO)
        Fastbootd.__init__(self, logger=fastbootd_logger)

    def gen_mac_address(self, serial):
        mac_addr = bytearray(hashlib.sha1(serial.encode()).digest()[-7:-1])

        # Ensure we generated a locally-administrated MAC address
        if (mac_addr[0] & 0x0F) not in [0x2, 0x6, 0xA, 0xE]:
            mac_addr[0] = (mac_addr[0] & 0x0F) | 0x2

        return MacAddress(bytes(mac_addr))

    def new_device_found(self, device):  # pragma: nocover
        # Ensure the bootloader is unlocked
        if device.variables.get("unlocked") != "yes":
            try:
                device.run_cmd(b"flashing:unlock")
            except Exception:
                # Ignore errors, we'll just tell users that the device is unusable
                pass

        # Ensure MaRS is aware of this client request
        serialno = device.variables["serialno"]
        mac_addr = self.gen_mac_address(serialno)
        product_name = device.variables.get("product")
        base_name = product_name or serialno
        ip_address, _ = self.boots.dhcpd.get_or_assign_ip_for_client(mac_addr)
        dut_data = {
            "id": serialno,
            "mac_address": mac_addr.as_str,
            "base_name": f"fastboot-{base_name}".lower(),
            "ip_address": ip_address,
            "tags": []
        }
        self.boots.mars.machine_discovered(dut_data, update_if_already_exists=False)

        # We got everything we wanted from the device, free our lock
        device.release()

        # Notify the DUT that we are ready to boot!
        if dut := self.boots.mars.get_machine_by_id(serialno):
            dut.handle_fastboot_device_added()

    def run(self):
        while True:
            self.update_device_list()
            time.sleep(1)


class BootService:
    def __init__(self, mars,
                 private_interface=None):
        self.mars = mars
        self.private_interface = private_interface or config.PRIVATE_INTERFACE

        # Do not start the servers
        if config.BOOTS_DISABLE_SERVERS:
            self.dhcpd = self.tftpd = self.fastbootd = None
            return

        self.dhcpd = BootsDHCPD(self, "DHCP Server", self.private_interface)
        self.dhcpd.dns_servers = [self.dhcpd.ip]
        self.dhcpd.start()

        self.tftpd = BootsTFTPD(self, "TFTP Server", None, self.private_interface)
        self.tftpd.start()

        self.fastbootd = BootsFastbootd(self, "Fastbootd Server")
        self.fastbootd.start()
