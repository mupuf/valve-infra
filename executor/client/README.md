# CI-tron's executor client

This client allow to interact with the ci-tron executor.

Take a look to our full documentation at
[https://gfx-ci.pages.freedesktop.org/ci-tron/](https://gfx-ci.pages.freedesktop.org/ci-tron/)
