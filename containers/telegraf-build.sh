#!/bin/bash
set -ex

. containers/build_functions.sh

build() {
    buildcntr=$(buildah from --isolation=chroot $EXTRA_BUILDAH_FROM_ARGS docker://alpine:${ALPINE_VERSION})
    buildmnt=$(buildah mount $buildcntr)

    local telegraf_arch
    case $platform in
        linux/amd64)
            telegraf_arch="amd64"
            ;;
        linux/arm64*)
            telegraf_arch="arm64"
            ;;
        linux/arm*)
            telegraf_arch="armel"
            ;;
        linux/riscv64)
            telegraf_arch="riscv64"
            ;;
    esac

    $buildah_run $buildcntr sh -c "wget -O - https://dl.influxdata.com/telegraf/releases/telegraf-${TELEGRAF_VERSION}_linux_${telegraf_arch}.tar.gz | tar xzf - --strip-components 2"

    cp -a containers/telegraf.conf $buildmnt/etc/telegraf/
    cp -a containers/telegraf-extra-inputs.sh $buildmnt/usr/local/bin/telegraf-extra-inputs.sh

    $buildah_run $buildcntr apk --no-cache add smartmontools nvme-cli bash lm-sensors

    buildah config --entrypoint '/usr/bin/telegraf' $buildcntr
}

build_and_push_container
