To build the documentation locally, run:

    $  make html

    or

    $ make singlehtml

Make sure your system has sphinx and sphinx_rtd_theme installed
or install them directly with pip:

    $ pip install -U sphinx sphinx-rtd-theme
