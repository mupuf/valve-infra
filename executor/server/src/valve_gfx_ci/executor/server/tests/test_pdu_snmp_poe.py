import pytest
from server.pdu.drivers.snmp_poe import SnmpPoePDU, SnmpPoeTpLinkPDU
from server.tests.test_pdu_basesnmp import _reset_easysnmp_mock


@pytest.fixture(autouse=True)
def reset_easysnmp_mock(monkeypatch):
    global Session, time_sleep
    Session, time_sleep = _reset_easysnmp_mock(monkeypatch)


def test_driver_SnmpPoePDU_default_min_off_time():
    assert SnmpPoePDU("MyPDU", config={"hostname": "127.0.0.1"}).default_min_off_time == 10.0


def test_driver_SnmpPoeTpLinkPDU_default_min_off_time():
    assert SnmpPoeTpLinkPDU("MyPDU", config={"hostname": "127.0.0.1"}).default_min_off_time == 10.0
