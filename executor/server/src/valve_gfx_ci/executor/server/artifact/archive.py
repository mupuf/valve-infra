from dataclasses import dataclass
from enum import StrEnum, auto
from io import FileIO
from pathlib import PurePath
from typing import Optional

import hashlib
import os
import re
import sys
import traceback

from libarchive import Archive, Entry, _libarchive
from .io import ArtifactIOBase


class ArchiveFormat(StrEnum):
    NONE = auto()
    CPIO = auto()
    ISO = auto()
    TAR = auto()
    ZIP = auto()


class ArchiveCompression(StrEnum):
    NONE = auto()
    GZ = auto()
    BZ2 = auto()


@dataclass
class ArtifactKeep:
    path: re.Pattern
    rewrite: Optional[str] = None

    def matches(self, path) -> str:
        if self.path.fullmatch(path):
            if self.rewrite:
                return self.path.sub(self.rewrite, path)
            else:
                return path


class EntryNotFound(ValueError):
    pass


class ArchiveArtifact(FileIO, ArtifactIOBase):
    def __init__(self, artifact: ArtifactIOBase, format: ArchiveFormat, compression: ArchiveCompression,
                 keep: list[ArtifactKeep]):
        def find_match(pathname: str) -> str:
            for match in keep:
                if rewritten_path := match.matches(pathname):
                    return match, rewritten_path

            return None, None

        def stream_entry(entry, write):
            if entry.symlink == '':
                for chunk in a.readstream(entry.size):
                    write(chunk)
                    etag.update(chunk)

        # Wait for the artifact to be done downloading since libarchive won't
        # call the emulated read function and thus requires all the data to be
        # present before parsing
        artifact.wait_for_complete()

        with Archive(artifact, "r") as a:
            memfd = FileIO(os.memfd_create("ArchiveArtifact"), "wb", closefd=False)

            try:
                etag = hashlib.blake2b(digest_size=16)

                output_archive = None
                if format != ArchiveFormat.NONE:
                    filter = compression.value if compression and compression != ArchiveCompression.NONE else None
                    output_archive = Archive(memfd, "w", format=format.value, filter=filter)

                # Find a matching file in the archive
                written_dir_entries = {".": None, "/": None}
                is_empty = True
                for entry in a:
                    # Try to find a match for the entry
                    _, rewritten_path = find_match(entry.pathname)
                    if rewritten_path:
                        is_empty = False
                        if output_archive:
                            entry.pathname = rewritten_path

                            # Make sure all the parent folders exist, going from the root to the leaf
                            for parent in reversed(PurePath(entry.pathname).parents):
                                parent = str(parent)
                                if parent not in written_dir_entries:
                                    parent_entry = Entry(pathname=parent, size=0, mtime=entry.mtime, mode=0o40755)
                                    parent_entry.to_archive(output_archive)
                                    written_dir_entries[parent_entry.pathname] = parent_entry

                            # Keep track of all the folders we have written out
                            if entry.isdir():
                                written_dir_entries[rewritten_path.removesuffix("/")] = entry

                            # Add the file to the new archive
                            entry.to_archive(output_archive)
                            stream_entry(entry, lambda data: _libarchive.archive_write_data_from_str(output_archive._a,
                                                                                                     data))
                            _libarchive.archive_write_finish_entry(output_archive._a)
                        else:
                            if entry.symlink:
                                raise EntryNotFound("Cannot use a symlink as an artifact")

                            # Just stream the artifact directly to the memfd, then stop looking for a match
                            stream_entry(entry, memfd.write)
                            break

                if is_empty:
                    raise EntryNotFound("Couldn't find a matching entry")

                # Finish writing the archive, then reset the file
                if output_archive:
                    output_archive.close()
                memfd.flush()
                memfd.seek(0)
                self._etag = etag.hexdigest()
                super().__init__(memfd.fileno(), mode='r', closefd=True)
            except Exception as e:
                if not isinstance(e, EntryNotFound):  # pragma: nocover
                    traceback.print_exc(file=sys.stderr)
                memfd.close()
                raise e from None

    @property
    def content_type(self) -> str:
        return "application/octet-stream"

    @property
    def is_complete(self) -> bool:
        return True

    @property
    def etag(self) -> str:
        return self._etag
