stages:
  - prep
  - tests
  - containers
  - integration testing
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  FDO_UPSTREAM_REPO: gfx-ci/ci-tron

  # GATEWAY_IMAGE_TAG must always be bumped when bumping GATEWAY_BASE_IMAGE_TAG
  GATEWAY_BASE_IMAGE_TAG: 2024-12-12.1
  GATEWAY_IMAGE_TAG: 2025-02-01.1

  # If no variables were set in the project's CI variables, default to using fd.o's runners
  # See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence for more details
  AARCH64_RUNNER_TAG_ARCH: aarch64
  AARCH64_RUNNER_TAG_EXTRA: ''
  AARCH64_RUNNER_TAG_EXTRA2: ''
  AMD64_RUNNER_TAG_EXTRA: ''
  AMD64_RUNNER_TAG_ARCH: ''
  AMD64_RUNNER_TAG_EXTRA2: ''

include:
  - project: 'freedesktop/ci-templates'
    ref: dd90ac0d7a03b574eb4f18d7358083f0c97825f3
    file:
      - '/templates/fedora.yml'

# Use the x86_64 runners by default
default:
  tags:
    - $AMD64_RUNNER_TAG_EXTRA
    - $AMD64_RUNNER_TAG_EXTRA2
    - $AMD64_RUNNER_TAG_ARCH

# Only run jobs post-merge or in merge requests
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# Job execution rules

.container-build files: &container_build_files
  - .gitlab-ci.yml
  - containers/**/*

.test integration files: &integration_testing_files
  - ansible/**/*
  - executor/**/*
  - salad/**/*
  - vivian/**/*
  - Makefile
  - .gitlab-ci.yml

#### CONTAINER BUILD TEMPLATE ####

.container-build-vars:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2023-02-02.0
  variables:
    FDO_REPO_SUFFIX: $CI_JOB_NAME
  rules:
    # When running post-merge in the default branch, always run the container
    # jobs and tag the latest containers as `latest`
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *container_build_files
      variables:
        FDO_DISTRIBUTION_TAG_LATEST: latest
    - changes: *container_build_files

.container-build-template:
  extends: .container-build-vars
  dependencies: []  # Do not download artifacts from previous stages
  script: containers/$CI_JOB_NAME-build.sh

.combinable-container-build-template:
  # NOTE: Jobs inheriting from this job need to be named `$IMAGE_NAME:$ARCH`
  # The following variables need to be set:
  # - ARCH: The short name to identify the architecture of the container (will get appended to the image tag)
  # - IMAGE_TAG: Tag of the container you want to create, once combined
  # - RUNNER_TAG_ARCH: Runner tag specifying the architecture of the machine where this job needs to run
  # - RUNNER_TAG_EXTRA: Runner tag specifying the type of machine where this job needs to run
  image: registry.freedesktop.org/freedesktop/ci-templates/$ARCH/container-build-base:2023-02-02.0
  dependencies: []  # Do not download artifacts from previous stages
  variables:
    FDO_DISTRIBUTION_TAG: "$IMAGE_TAG-$ARCH"
  rules:
    - changes: *container_build_files
  script:
    - export FDO_REPO_SUFFIX=$(echo ${CI_JOB_NAME} | cut -d ':' -f 1)
    - containers/${FDO_REPO_SUFFIX}-build.sh

.combinable-container-build-template-x86_64:
  extends:
    - .combinable-container-build-template
  tags:
    - $AMD64_RUNNER_TAG_ARCH
    - $AMD64_RUNNER_TAG_EXTRA
    - $AMD64_RUNNER_TAG_EXTRA2
  variables:
    ARCH: x86_64

.combinable-container-build-template-aarch64:
  extends:
    - .combinable-container-build-template
  tags:
    - $AARCH64_RUNNER_TAG_ARCH
    - $AARCH64_RUNNER_TAG_EXTRA
    - $AARCH64_RUNNER_TAG_EXTRA2
  variables:
    ARCH: aarch64

.combinable-container-combine-template:
  # NOTE: Jobs inheriting from this job need to be named `$IMAGE_NAME:combine`
  # The following variables need to be set:
  # - IMAGE_TAG: Tag of the container you want to create by combining all existing tags that start with this one
  image: !reference [.container-build-vars, image]
  dependencies: []  # Do not download artifacts from previous stages
  rules:
    - changes: *container_build_files
  script: |
    set -x

    # log in to the registry
    podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    IMAGE_NAME=$CI_REGISTRY_IMAGE/$(echo ${CI_JOB_NAME} | cut -d ':' -f 1)
    IMAGE_FULL_LABEL=$IMAGE_NAME:$IMAGE_TAG
    for TAG in $(skopeo list-tags docker://$IMAGE_NAME | jq -r '.Tags[]'); do
        # Add any image tag that has $IMAGE_TAG as a prefix
        if [[ "$TAG" = "$IMAGE_TAG"-* ]]; then
            IMAGES="$IMAGE_NAME:$TAG ${IMAGES}"
        fi
    done

    # create the multi-arch manifest
    MANIFEST_LABEL=localhost/${IMAGE_FULL_LABEL}
    manifest_id=$(buildah manifest create ${MANIFEST_LABEL} ${IMAGES})

    # push the manifest
    buildah manifest push --all ${MANIFEST_LABEL} docker://${IMAGE_FULL_LABEL}

    # tag the container as latest too, if we are running on the default branch (post merge)
    if [ -n "$CI_COMMIT_BRANCH" ] && [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
        # NOTE: running on the default branch. CI_COMMIT_BRANCH is *not* set in MRs
        buildah manifest push --all ${MANIFEST_LABEL} docker://$IMAGE_NAME:latest
    fi

    # delete the image, we won't need it
    buildah rmi "$manifest_id"

#### BOOTSTRAP CONTAINERS ####

python-container:
  stage: prep
  extends:
    - .fdo.container-build@fedora
  variables:
    # We need a build toolchain because easysnmp in unmaintained, and
    # hence has no binary wheel for us to leverage.
    FDO_DISTRIBUTION_PACKAGES: 'gcc net-snmp-devel python3-devel python3-pip wget libarchive-devel'
    FDO_DISTRIBUTION_VERSION: '40'
    FDO_REPO_SUFFIX: 'python-container'
    FDO_DISTRIBUTION_TAG: '2024-09-26'
    FDO_DISTRIBUTION_EXEC: 'pip install --break-system-packages build tox twine flake8 sphinx==5.3.0 sphinx-rtd-theme sphinxcontrib-mermaid'

.set-python-image:
  variables:
    TAG: !reference [python-container, variables, FDO_DISTRIBUTION_TAG]
  image: $CI_REGISTRY_IMAGE/python-container:$TAG
  needs:
    - job: python-container
      artifacts: false

.gateway-base:
  stage: prep
  rules:
    - changes: *container_build_files
    - changes: *integration_testing_files
  variables:
    BASE_IMAGE: fedora:41
    IMAGE_TAG: $GATEWAY_BASE_IMAGE_TAG

gateway-base:x86_64:
  extends:
    - .combinable-container-build-template-x86_64
    - .gateway-base

gateway-base:aarch64:
  extends:
    - .combinable-container-build-template-aarch64
    - .gateway-base

#### TESTS ####

test ansible:
  image: $CI_REGISTRY_IMAGE/gateway-base:$GATEWAY_BASE_IMAGE_TAG-x86_64
  stage: tests
  needs:
    - job: gateway-base:x86_64
      artifacts: false
      optional: true
  script: |
    set -eux

    # The Gitlab runner cache deliberately chmod 777's all
    # directories. This upsets ansible and there's nothing we can
    # really do about it in our repo. See
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4187
    chmod -R o-w ansible
    cd ansible
    ansible-lint --version
    ansible-lint -x yaml[line-length] -x yaml[braces] -x yaml[commas] -x name[casing] -x var-naming[no-role-prefix] --strict
    ansible-playbook --syntax-check gateway.yml
  rules:
    - changes:
        - ansible/**/*
        - .gitlab-ci.yml

.python-test:
  extends:
    - .set-python-image
  stage: tests
  needs:
    - job: python-container
      artifacts: false
  rules:
    - changes: &python_test_files
        - ${PACKAGE_DIR}/**/*
        - .gitlab-ci.yml
  script:
    # HACK: Remove when setuptools reverts their regression
    # https://github.com/pypa/setuptools/issues/4519
    - echo 'setuptools<72' > /tmp/constraints.txt
    - export PIP_CONSTRAINT=/tmp/constraints.txt

    - tox -c ${PACKAGE_DIR}/setup.cfg
    - python3 -m build ${PACKAGE_DIR}
  artifacts:
    paths:
      - ${PACKAGE_DIR}/dist/

tox:
  extends:
    - .python-test
  parallel:
    matrix:
      - PACKAGE_DIR:
          - executor/client
          - executor/server
          - salad
          - ipxe-boot-server

test gfxinfo:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'gfxinfo'
  rules:
    # Always run the full set of tests when run in a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"

    # Make sure that any merge request that affects gfxinfo runs the full set of tests
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - gfxinfo/**/*

    # Skip the DB completeness tests for any other scenario
    - changes: *python_test_files
      variables:
        GFXINFO_SKIP_DB_COMPLETENESS_CHECK: 1

  # NOTE: We can't re-use the script from .python-test, as the only way to
  # execute code after a script is using `after_script`... which can't be used
  # to fail a job. Instead, we use yaml's ability to reference other sections
  # and use that to inline the original code, thus preventing code duplication
  # that could get out of sync.
  script:
    - |
      # Ensure we cache all databases by installing the package as editable, then checking the DB
      pip install --break-system-packages -e ${PACKAGE_DIR}/
      gfxinfo --check-db

    # Include the original .python-test script, for consistency with the other
    # python test jobs.
    - !reference [.python-test, script]

    - |
      # Disable internet access and check that the package works without it
      echo '' > /etc/resolv.conf
      pip install --break-system-packages ${PACKAGE_DIR}/dist/*.whl
      gfxinfo --check-db

test ansible py files:
  extends:
    - .set-python-image
  stage: tests
  needs:
    - job: python-container
      artifacts: false
  rules:
    - changes:
        - ansible/**/*.py
        - .gitlab-ci.yml
  script:
    - flake8 --max-line-length=130 $(find ansible/ -name '*.py')

test pages:
  extends:
    - .set-python-image
  stage: tests
  needs:
    - job: python-container
      artifacts: false
  rules:
    - changes:
        - documentation/**/*
        - .gitlab-ci.yml
  script:
    - sphinx-build -W -b html documentation/ public/
    - sphinx-build -W -b singlehtml documentation/ singlehtml/
  artifacts:
    paths:
      - public/
      - singlehtml/

#### PUBLIC CONTAINERS: ####

#
# WARNING: Do not change the name of the jobs, as it will also change the name
# of the container...

mesa-trigger:
  stage: containers
  extends:
    - .container-build-template
  variables:
    FDO_DISTRIBUTION_TAG: '2024-01-05.1'
    FDO_DISTRIBUTION_PLATFORMS: 'linux/arm64/v8 linux/amd64'

machine-registration:
  stage: containers
  extends:
    - .container-build-template
  needs:
    - job: test gfxinfo
      optional: true
  rules:
    # When running post-merge in the default branch, always run the container
    # jobs and tag the latest containers as `latest`
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *container_build_files
      variables:
        FDO_DISTRIBUTION_TAG_LATEST: latest
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *integration_testing_files
      variables:
        FDO_DISTRIBUTION_TAG_LATEST: latest

    # We were not post-merge, try again to match the file changes
    - changes: *container_build_files
    - changes: *integration_testing_files
  variables:
    FDO_DISTRIBUTION_TAG: '2025-02-15.1'
    FDO_DISTRIBUTION_PLATFORMS: 'linux/riscv64 linux/arm/v6 linux/arm64/v8 linux/amd64'
    # FIXME: Move to ci-templates when multi-arch support lands

telegraf:
  stage: containers
  extends:
    - .container-build-template
  variables:
    ALPINE_VERSION: "3.20"
    TELEGRAF_VERSION: "1.32.1"
    FDO_DISTRIBUTION_TAG: "2022-10-18.1"
    FDO_DISTRIBUTION_PLATFORMS: 'linux/arm/v6 linux/arm64/v8 linux/amd64'
.gateway:
  stage: containers
  variables:
    BASE_IMAGE: $CI_REGISTRY_IMAGE/gateway-base:$GATEWAY_BASE_IMAGE_TAG-$ARCH
    IMAGE_TAG: $GATEWAY_IMAGE_TAG

gateway:x86_64:
  extends:
    - .combinable-container-build-template-x86_64
    - .gateway

gateway:aarch64:
  extends:
    - .combinable-container-build-template-aarch64
    - .gateway

#### Integration testing ####

test integration:
  variables:
    ANSIBLE_CONFIG: $CI_PROJECT_DIR/ansible/ansible.cfg
    MACHINE_REGISTRATION_TAG: !reference [machine-registration, variables, FDO_DISTRIBUTION_TAG]
    REGISTRY: $CI_REGISTRY
  image: $CI_REGISTRY_IMAGE/gateway-base:$GATEWAY_BASE_IMAGE_TAG-x86_64
  stage: integration testing
  needs:
    - job: 'test integration:runner job:generate'
      artifacts: false
    - job: gateway-base:x86_64
      artifacts: false
    - job: machine-registration
      artifacts: false
  rules:
    # When running on CI-tron gateways, optimize boot times by using the hosts' pull-through registry
    - if: $AMD64_RUNNER_TAG_EXTRA == "CI-gateway" || $AMD64_RUNNER_TAG_EXTRA2 == "CI-gateway"
      changes: *integration_testing_files
      variables:
        REGISTRY: 10.42.0.1:8002

    - changes: *integration_testing_files
  before_script: |
    pip install ./executor/client
  script: |

    shellcheck vivian/vivian

    # NOTE: local registry is skipped since starting it from within the gitlab
    # runner container doesn't work. On CI-tron gateways, we can use  and there's already a local registry
    # running on the runner (since runners are gateways)
    #
    # NOTE 2: We source the initial gateway container from the upstream project
    # because a fresh fork would not have the gateway container built/tagged in
    # their registry, and building/tagging the container before testing live-
    # deploying the potential changes made on top of upstream's main is important
    # before merging any MR.

    ./containers/run_net.sh make -j4 \
      VIVIAN_OPTS="--console-file=stdout --tests-run-ansible --machine-registration-container=10.42.0.1:8002/$CI_PROJECT_PATH/machine-registration:$MACHINE_REGISTRATION_TAG" \
      SKIP_LOCAL_REGISTRY=1 \
      IMAGE_NAME="$REGISTRY/$FDO_UPSTREAM_REPO/gateway:latest" \
      FARM_NAME="integration-test-$CI_PROJECT_PATH_SLUG-gateway" \
      vivian-integration-tests

  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - artifacts/

test integration:runner job:generate:
  stage: integration testing
  variables:
    GIT_STRATEGY: none
  image: $CI_REGISTRY_IMAGE/mesa-trigger:2024-01-05.1
  needs: []
  rules:
    - changes: *integration_testing_files
  script:
    # For use by the job define in the previous command
    - mkdir shared_folder
    - echo file1 > shared_folder/file1
    - echo file2 > shared_folder/file2
  artifacts:
    paths:
      - shared_folder

test integration:runner job:
  stage: integration testing
  variables:
    GIT_STRATEGY: none
  image: registry.freedesktop.org/gfx-ci/ci-tron/mesa-trigger:latest
  tags:
    - ci-tron-integration-test-$CI_PROJECT_ID-$CI_PIPELINE_IID
  needs:
    - job: 'test integration:runner job:generate'
      artifacts: true
  rules:
    - changes: *integration_testing_files
  script:
    - cat shared_folder/file1 shared_folder/file2 | tee shared_folder/file3
  artifacts:
    paths:
      - shared_folder


#### DEPLOYMENT JOBS ####

gateway-base:combine:
  stage: deploy
  extends:
    - .combinable-container-combine-template
  variables:
    IMAGE_TAG: $GATEWAY_BASE_IMAGE_TAG

gateway:combine:
  stage: deploy
  extends:
    - .combinable-container-combine-template
  variables:
    IMAGE_TAG: $GATEWAY_IMAGE_TAG

.python-deploy:
  extends:
    - .set-python-image
  stage: deploy
  rules:
    - if: $TWINE_PASSWORD == ""
      when: never
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${PACKAGE_DIR}/setup.cfg
      when: on_success
  script:
    - twine upload -u "__token__" ${PACKAGE_DIR}/dist/*
  # This will fail if the package already exists, not pleasant to
  # check ahead of time for that, so allow the job to fail
  allow_failure: true

deploy executor/client:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_EXECUTOR_CLIENT_TOKEN
    PACKAGE_DIR: 'executor/client'
  needs:
    - job: tox
      parallel:
        matrix:
          - PACKAGE_DIR: executor/client

deploy executor/server:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_EXECUTOR_SERVER_TOKEN
    PACKAGE_DIR: 'executor/server'
  needs:
    - job: tox
      parallel:
        matrix:
          - PACKAGE_DIR: executor/server

deploy gfxinfo:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_GFXINFO_TOKEN
    PACKAGE_DIR: gfxinfo
  needs:
    - test gfxinfo

deploy salad:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_SALAD_TOKEN
    PACKAGE_DIR: salad
  needs:
    - job: tox
      parallel:
        matrix:
          - PACKAGE_DIR: salad

# IMPORTANT: The name of this job must be "pages" or it won't update the pages
# Builds and deploy documentation at https://gfx-ci.pages.freedesktop.org/ci-tron/
pages:
  extends:
    - .set-python-image
  stage: deploy
  needs:
    - test pages
  rules:
    # Only run when running on the default branch, post-merge
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH || $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - !reference [test pages, rules]
  script:
    - ls public/
  artifacts:
    paths:
      - public/
